import urlUtils from '../../components/utils/url';
import initLazyLoad from '../../components/utils/lazyLoadHelper';
import filtersHelper from '../../components/utils/filtersHelper';
import tilesHelper from '../../components/utils/tilesHelper';
import initEvents from '../../components/events';

const GROUP_FILTER = 'group',
    INDUSTRY_FILTER = 'industry';

initLazyLoad();

$(document).ready(() => {
    var {groupParam, industryParam} = urlUtils.getParamsOnInit(GROUP_FILTER, INDUSTRY_FILTER);

    if (groupParam || industryParam) {
        tilesHelper.updateTiles(groupParam, industryParam);
        filtersHelper.handleFilters(groupParam, industryParam);
    } else {
        tilesHelper.renderTiles();
        filtersHelper.handleFilters();
    }
    initEvents();
});
