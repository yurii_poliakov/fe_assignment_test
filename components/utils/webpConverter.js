const imagemin = require("imagemin");
const webp = require("imagemin-webp");

(() => {
	imagemin(['./img/*.png'], {
		destination: './img/',
		plugins: [
			webp()
		]
	});
})();