import service from './service';

function validateValue(value, config) {
    if (value) {
        if (config.minLength && value.length < config.minLength) {
            return config.minLengthText;
        } else if (config.maxLength && value.length > config.maxLength) {
            return config.maxLengthText;
        } else if (config.regEx && !(new RegExp(config.regEx, 'g')).test(value)) {
            return config.parseError;
        }
    } else if (config.isRequired) {
        return config.requiredText;
    }
    return '';
}

function setError ($el, error) {
    var $inputWrapper = $el.closest('.js-input-wrapper');
    $inputWrapper.find('.text-error').remove();

    if (error) {
        $el.after(
            `<div class="text-error">
                ` + $('<div>').text(error).html() /*escapes content*/ + `
            </div>`
        );
    }
}

function validateInput (el) {
    var $el = $(el);
    var value = $el.val().trim();
    var config = $el.data();
    var validationResult = validateValue(value, config);
    setError($el, validationResult);

    return !validationResult;
}

function handleFormState($form, errorMsg, isError) {
    $form.find('.text-error').remove();
    $form.find('.form-success').remove();

    if (!isError) {
        $form.append('<div class="form-success">Your message has sent. Thank you!</div>');
        $form[0].reset();
        setTimeout(() => {
            $form.find('.form-success').remove();
        }, 3000);
    } else {
        $form.append(
            `<div class="text-error">
            ` + $('<div>').text(errorMsg).html() /*escapes content*/ + `
            </div>`);
    }
}

function onSubmit(e) {
    e.preventDefault();
    var $form = $(e.currentTarget);
    var $childs = $form.find('.js-input');
    var isValid = false;

    $childs.each((idx, el) => {
        isValid = validateInput(el);
    });

    if (isValid) {
        service.sendData().then((response) => {
            handleFormState($form, response.errorMsg, response.isError);
        });
    }
}

export default {validateInput, onSubmit};

