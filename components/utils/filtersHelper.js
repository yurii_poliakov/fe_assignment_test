import filtersTemplate from '../../templates/filters.hbs';
import filters from '../../data/filters';
import tiles from '../../data/tiles';

const ALL_FILTERS = 'all',
    GROUP_FILTER = 'group',
    INDUSTRY_FILTER = 'industry';

function updateFilters(filtersArr, tilesArr, filter) {
    return filtersArr.filter((filterItem) => {
        if (filterItem.value === ALL_FILTERS) {
            return true;
        }
        return tilesArr.some((tile) => {
            return tile[filter] === filterItem.value;
        });
    });
}

function handleFilters(groupSelectedValue, industrySelectedValue) {
    var $filtersWrapper = $('.filters');

    var groups = updateFilters(filters.GROUPS_FILTERS, tiles.items, GROUP_FILTER);
    if (groupSelectedValue) {
        groups.forEach(filter => {
            if (filter.value === groupSelectedValue) {
                filter.selected = true;
            }
        });
    }

    var industries = updateFilters(filters.INDUSTRIES_FILTERS, tiles.items, INDUSTRY_FILTER);
    if (industrySelectedValue) {
        industries.forEach(filter => {
            if (filter.value === industrySelectedValue) {
                filter.selected = true;
            }
        });
    }

    var filtersHtml = filtersTemplate({groups, industries});
    $filtersWrapper.html(filtersHtml);
}

function getSelectedFilterValues() {
    var groupFilterValue = $('.groups option:selected').val();
    var industryFilterValue = $('.industries option:selected').val();
    return {groupFilterValue, industryFilterValue};
}

export default {handleFilters, getSelectedFilterValues};