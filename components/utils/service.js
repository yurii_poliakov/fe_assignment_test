function sendData() {
    var isError = !!Math.floor(Math.random() * 2);
    var response = {
        isError,
        errorMsg: isError ? 'Some of your data are invalid. Please correct it, and resubmit the form' : ''
    };

    return Promise.resolve(response);
}

export default {sendData};