import LazyLoad from 'vanilla-lazyload';

const LAZY_SELECTOR = '.lazy';

export default function initLazyLoad() {
    window.lazyloadInstance = new LazyLoad({
        elements_selector: LAZY_SELECTOR,
        threshold: 100
    });
}