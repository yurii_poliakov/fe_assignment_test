import tilesTemplate from '../../templates/list.hbs';
import tiles from '../../data/tiles';

const ALL_FILTERS = 'all';

function renderTiles(data = tiles) {
    var $list = $('.list');
    var tilesHtml = tilesTemplate(data);
    $list.html(tilesHtml);
    window.lazyloadInstance.update();
}

function updateTiles(groupFilterValue = ALL_FILTERS, industryFilterValue = ALL_FILTERS) {
    var items = tiles.items.filter((item) => {
        return (item.group === groupFilterValue || groupFilterValue === ALL_FILTERS) &&
            (item.industry === industryFilterValue || industryFilterValue === ALL_FILTERS);
    });

    renderTiles({items});
}

export default {renderTiles, updateTiles};
