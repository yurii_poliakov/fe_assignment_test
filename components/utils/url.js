function appendParamToURL (url, name, value) {
    if (url.indexOf(name + '=') !== -1) {
        return url;
    }
    var separator = url.indexOf('?') !== -1 ? '&' : '?';
    return url + separator + name + '=' + encodeURIComponent(value);
}

function removeParamFromURL (url, name) {
    if (url.indexOf('?') === -1 || url.indexOf(name + '=') === -1) {
        return url;
    }
    var hash;
    var params;
    var domain = url.split('?')[0];
    var paramUrl = url.split('?')[1];
    var newParams = [];
    if (paramUrl.indexOf('#') > -1) {
        hash = paramUrl.split('#')[1] || '';
        paramUrl = paramUrl.split('#')[0];
    }
    params = paramUrl.split('&');
    for (var i = 0; i < params.length; i++) {
        if (params[i].split('=')[0] !== name) {
            newParams.push(params[i]);
        }
    }

    var paramsString = newParams.length > 0 ? '?' + newParams.join('&') : '';

    return domain + paramsString + (hash ? '#' + hash : '');
}

function replaceParameterInUrl (url, name, value) {
    url = removeParamFromURL(url, name);
    return appendParamToURL(url, name, value);
}

function getQueryStringParams (qs) {
    if (!qs || qs.length === 0) {
        return {};
    }
    var params = {};
    qs.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
        ($0, $1, $2, $3) => {
            params[$1] = decodeURIComponent($3).replace(/\+/g, ' ');
        }
    );
    return params;
}

function getParameterValueFromUrl (parameterName, url) {
    var currentQueryString = url || window.location.search;
    var currentQueryStringParams = getQueryStringParams(currentQueryString);

    return currentQueryStringParams[parameterName];
}

function updateUrl(groupValue, insdustryValue, groupParam, industryParam) {
    var url = window.location.href;
    url = replaceParameterInUrl(url, groupParam, groupValue);
    url = replaceParameterInUrl(url, industryParam, insdustryValue);
    window.history.pushState(null, null, url);
}

function getParamsOnInit(group, industry) {
    var url = window.location.href;
    var groupParam = getParameterValueFromUrl(group, url);
    var industryParam = getParameterValueFromUrl(industry, url);
    return {groupParam, industryParam};
}

export default {
    appendParamToURL,
    removeParamFromURL,
    replaceParameterInUrl,
    getQueryStringParams,
    getParameterValueFromUrl,
    updateUrl,
    getParamsOnInit
};
