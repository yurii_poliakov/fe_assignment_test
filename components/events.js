import filtersHelper from '../components/utils/filtersHelper';
import urlUtils from '../components/utils/url';
import formsHelper from './utils/formsHelper';
import tilesHelper from '../components/utils/tilesHelper';

const GROUP_FILTER = 'group',
    INDUSTRY_FILTER = 'industry';

export default function initEvents() {
    $('.filters').on('change', () => {
        var {groupFilterValue, industryFilterValue} = filtersHelper.getSelectedFilterValues();
        tilesHelper.updateTiles(groupFilterValue, industryFilterValue);
        urlUtils.updateUrl(groupFilterValue, industryFilterValue, GROUP_FILTER, INDUSTRY_FILTER);
    });

    $('.js-input').on('change', (e) => {
        formsHelper.validateInput(e.currentTarget);
    });

    $('.js-question-form').on('submit', (e) => {
        formsHelper.onSubmit(e);
    });

    $('.toggle-layout').on('click', () => {
        $('.list').toggleClass('d-flex').toggleClass('layout-list');
    });
}
