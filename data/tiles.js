const TILES = {
    items: [
        {
            name: 'BOL.com',
            about: 'A summer island in the Netherlands',
            img: './img/bolcom.png',
            webpImg: './img/bolcom.webp',
            industry: 'b2b',
            group: 'branding'
        },
        {
            name: 'KEMPEN',
            about: 'Not some average banking website',
            img: './img/kempen.png',
            webpImg: './img/kempen.webp',
            industry: 'b2b',
            group: 'branding'
        },
        {
            name: 'PHILIPS',
            about: 'Beautiful design meets innovative technology',
            img: './img/philips.png',
            webpImg: './img/philips.webp',
            industry: 'b2b',
            group: 'branding'
        },
        {
            name: 'Gemeentemuseum',
            about: 'A 100 years of Mondriaan & De Stijl',
            img: './img/gemeentemuseum.png',
            webpImg: './img/gemeentemuseum.webp',
            industry: 'health',
            group: 'commerce'
        },
        {
            name: 'Be Lighting',
            about: 'Delivering clarity on a global scale',
            img: './img/lightning.png',
            webpImg: './img/lightning.webp',
            industry: 'health',
            group: 'technology',
            isSpecialRight: true,
            linkedTiles: [
                {
                    name: 'BUTLINS',
                    about: 'Enhancing customer experience for personalised holiday planning using real time data'
                },
                {
                    name: 'VACANSESELECT',
                    about: 'Predicting booking behavior for holidays with smart data, voice search and machine learning'
                }
            ]
        },
        {
            name: 'ZALANDO',
            about: 'A summer island in the Netherlands',
            img: './img/zalando.png',
            webpImg: './img/zalando.webp',
            industry: 'health',
            group: 'commerce'
        },
        {
            name: 'TUI',
            about: 'Swipe to find your next holiday destination',
            img: './img/tui.png',
            webpImg: './img/tui.webp',
            industry: 'education',
            group: 'design'
        },
        {
            isQuote: true,
            author: 'MATTIJS TEN BRINK – CEO, TRANSAVIA',
            quote: `Dept helped us tell our story through a bold new identity and a robust online experience.
                To the tune of 60% growth in online bookings in just one month.`
        },
        {
            name: 'Chocomel',
            about: 'A campaign for the limited edition letter packs',
            img: './img/chocomel.png',
            webpImg: './img/chocomel.webp',
            industry: 'education',
            group: 'design',
            isSpecialLeft: true,
            linkedTiles: [
                {
                    name: 'MICROSOFT',
                    about: 'Tapping into Ireland’s unique gaming culture and bringing a fresh flavour to their Xbox social media channels'
                },
                {
                    name: 'O’NEILL',
                    about: 'Integrating existing content into O’Neill’s new e-commerce platform'
                }
            ]
        },
        {
            name: 'JBL',
            about: 'Live like a champion with Jerome Booteng',
            img: './img/jbl.png',
            webpImg: './img/jbl.webp',
            industry: 'media',
            group: 'design'
        }
    ]
};

export default TILES;
