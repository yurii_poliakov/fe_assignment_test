const GROUPS_FILTERS = [     
    {
        value: 'all',
        displayValue: 'all work'
    },
    {
        value: 'technology',
        displayValue: 'technology'
    },
    {
        value: 'strategy',
        displayValue: 'strategy'
    },
    {
        value: 'design',
        displayValue: 'design'
    },
    {
        value: 'advertising',
        displayValue: 'advertising'
    },
    {
        value: 'commerce',
        displayValue: 'commerce'
    },
    {
        value: 'media',
        displayValue: 'media'
    },
    {
        value: 'branding',
        displayValue: 'branding'
    },
    {
        value: 'digital',
        displayValue: 'digital marketing'
    },
    {
        value: 'content',
        displayValue: 'content'
    },
    {
        value: 'data',
        displayValue: 'data & intelligence'
    },
    {
        value: 'marketing',
        displayValue: 'marketing'
    }
];

const INDUSTRIES_FILTERS = [
    {
        value: 'all',
        displayValue: 'all industries'
    },
    {
        value: 'b2b',
        displayValue: 'b2b'
    },
    {
        value: 'retail',
        displayValue: 'retail & fashion'
    },
    {
        value: 'finance-insurance',
        displayValue: 'finance & insurance'
    },
    {
        value: 'utilities',
        displayValue: 'utilities'
    },
    {
        value: 'fast-moving-consumer-goods',
        displayValue: 'fast moving consumer goods'
    },
    {
        value: 'non-profit',
        displayValue: 'non profit'
    },
    {
        value: 'health',
        displayValue: 'health'
    },
    {
        value: 'education',
        displayValue: 'education'
    },
    {
        value: 'media',
        displayValue: 'media'
    },
    {
        value: 'travel',
        displayValue: 'travel'
    }
]

export default {GROUPS_FILTERS, INDUSTRIES_FILTERS};
